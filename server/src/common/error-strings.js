export default {
  vehicle: {
    reg_plate: `Expected string a valid Bulgarian license plate format: ([A-Z]X|XX)([0-9]XXXX)([A-Z]XX)`,
    year: `Expected year format XXXX - 1900 - 2099!`,
    class: `Expected string 'A', 'B', 'C', 'D', 'E', 'F', 'S'!`,
    vin: `Expected a valid exactly 17 long string BG vehicle identification number!`,
    transmissions: `Expected string 'manual', 'auto'!`,
    engine: `Expected string 'petrol', 'diesel', 'hybrid'!`,
    model: 'Expected string 1-20 length!',
    brand: 'Expected a valid brand from our garage ->see our brand list (39)!',
    customer_id: `Expected existing customer`,
  },
  users: {
    first_name: `Expected string with length [2-20]!`,
    last_name: `Expected string with length [2-20]!`,
    email: `Expected a valid email format!`,
    password: `Expected string with min length 8!`,
    phone_number: `Expected a valid phone format [(359)-882-974963, 541-753-6010, 541753-6010, or 541753-6010.
     359882974963, 359-882-974963, +359882974963, +359-882-974963]`,
    postal_code: `Expected a valid format XXXX (1000-9999) !`,
    country: `Expected string with length [3-45]!`,
    city: `Expected string with length [3-45]!`,
    street_address: `Expected string with length [3-45]!`,
  },
  login: {
    email: `Expected a valid email format!`,
    password: `Expected string with min length 8!`,
  },
  order: {
    status: `Expected string:'new', 'in progress'or 'done'`,
    notes: `Expected string with length [3-500]!`,
    issue_date: `Expected valid date`,
    due_date: `Expected valid date`,
    service_id: `Expected existing service`,
  },
  service: {
    name: `Expected string with length [3-45]!`,
    price: `Expected valid price(number)`,
  },
};
