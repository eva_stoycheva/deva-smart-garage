import mailjet from 'node-mailjet';

export const sendRegistrationEmail =(receiver, first_name, password)=>{
  const mail = mailjet
      .connect('35c32bf9be2e9d4bd665f098229f2a33', '16fd746bbc41c532faf9701fb2230912');
  const request = mail
      .post('send', { 'version': 'v3.1' })
      .request({
        'Messages': [
          {
            'From': {
              'Email': 'devasmartgarage@gmail.com',
              'Name': 'Deva Garage',
            },
            'To': [
              {
                'Email': receiver,
                'Name': first_name,
              },
            ],
            'Subject': 'Successfull Registration!',
            'HTMLPart': `You are registered at Deva Garage! Username: ${receiver} Password:${password}`,
            'CustomID': 'AppGettingStartedTest',
          },
        ],
      });
  request
      .then((result) => {
        console.log(result.body);
      })
      .catch((err) => {
        console.log(err.statusCode);
      });
};

export const sendResetPassWordEmail =(receiver, token, first_name, last_name)=>{
  const link = `http://localhost:3000/reset?id=${token}`;
  const mail = mailjet
      .connect('35c32bf9be2e9d4bd665f098229f2a33', '16fd746bbc41c532faf9701fb2230912');
  const request = mail
      .post('send', { 'version': 'v3.1' })
      .request({
        'Messages': [
          {
            'From': {
              'Email': 'devasmartgarage@gmail.com',
              'Name': 'Deva Garage',
            },
            'To': [
              {
                'Email': receiver,
                'Name': first_name,
              },
            ],
            'Subject': 'Forgotten password request',
            'HTMLPart': `
            Dear, ${first_name} ${last_name}! \n
            Deva Garage recently received a request for a forgotten password.\n
            Click on the following link in order to reset your password:\n
            ${link} \n
            If you did not request this change, you do not need to do anything.\n
            This link will expire in 1 hour.\n
          `,
            'CustomID': 'AppGettingStartedTest',
          },
        ],
      });
  request
      .then((result) => {
        console.log(result.body);
      })
      .catch((err) => {
        console.log(err.statusCode);
      });
};

