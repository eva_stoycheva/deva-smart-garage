import pool from './pool.js';
import { queryFuncSql, checkLengthSql } from '../common/helpers.js';

const getAll = async (data) => {
  const direction = data.sort || 'ASC';
  const startIndex = data.page ? (data.page - 1) * data.limit : 0;
  const sql = `
  SELECT id, name, price, is_deleted FROM services 
  WHERE is_deleted=0
  ORDER BY price ${direction}
  `;

  return await pool.query(sql);
};

const searchByQueries = async (data, serviceQueries) => {
  const direction = data.sort || 'ASC';

  const startIndex = data.page ? (data.page - 1) * data.limit : 0;
  const sql1 = `
  SELECT s.id, s.name, s.price, s.is_deleted FROM services s
  WHERE s.is_deleted=0 AND
  `;
  const sql2Arr = queryFuncSql(serviceQueries, 's');
  const sql2 = checkLengthSql(sql2Arr);

  const sqlQueries = sql1.concat(sql2).slice(0, -4);

  const sql3 = `ORDER BY price ${direction}`;

  const sql = sqlQueries + sql3;

  return await pool.query(sql);
};
const getBy = async (column, value, deleted = { is_deleted: 0 }) => {
  const sql = `
  SELECT id, name, price,is_deleted FROM services
  WHERE   ${column} =? And is_deleted=?
  `;
  return await pool.query(sql, [value, deleted.is_deleted]);
};

const deleteService = async (value) => {
  const sql = `UPDATE services SET is_deleted = 1 WHERE id = ?;
  `;
  return await pool.query(sql, [+value]);
};
const create = async (data) => {
  const sql = `INSERT INTO services (name, price) VALUES (?,?);
  `;

  return await pool.query(sql, [data.name, data.price]);
};

const update = async (dataBody, data) => {
  const sql = `UPDATE services SET price = ? WHERE id = ?;
  `;
  return await pool.query(sql, [dataBody.price, +data.id]);
};
export default {
  getAll,
  deleteService,
  getBy,
  create,
  update,
  searchByQueries,
};
