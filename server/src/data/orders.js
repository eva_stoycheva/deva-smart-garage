import pool from './pool.js';
import { queryFuncSql, checkLengthSql } from '../common/helpers.js';

const getAll = async (sort, date, page, limit) => {
  const direction = sort || 'ASC';
  const sql = `
  SELECT o.id as order_id , u.id as customer_id,CONCAT(u.first_name, " ",u.last_name) AS fullname, u.email,u.phone_number,u.role, u.is_deleted as user_is_deleted,
  a.id as addresses_id,a.street_address,a.city,a.country,a.postal_code ,v.id as vehicle_id,m.model_name,
  b.brand_name, v.reg_plate,v.year,v.class,v.vin,v.transmissions,v.engine,v.is_deleted as vehicles_is_deleted,
  o.issue_date, o.due_date,o.notes,o.status,o.is_deleted as order_is_deleted,GROUP_CONCAT(s.name) as service_name, sum(s.price) AS sum,CONCAT(empl.first_name, " ",empl.last_name) as employee_full_name
  FROM orders_has_services ord
  JOIN orders o ON o.id = ord.orders_id
  JOIN services s ON s.id = ord.services_id
  JOIN vehicles v ON v.id = o.vehicle_id
  JOIN users u ON u.id = v.customer_id
  JOIN users empl ON empl.id = o.users_id
  JOIN addresses a ON a.id = u.addresses_id
  JOIN models m ON m.id = v.models_id
  JOIN brands b ON b.id = m.brand_id
  Where o.is_deleted =0
  GROUP BY order_id
  ORDER BY  fullname ${direction}
  `;

  return await pool.query(sql);
};

const getByOrder = async (data) => {
  const sql = `
  SELECT o.id as order_id , u.id as customer_id,CONCAT(u.first_name, " ",u.last_name) AS fullname, u.email,u.phone_number,u.role, u.is_deleted as user_is_deleted,
  a.id as addresses_id,a.street_address,a.city,a.country,a.postal_code ,v.id as vehicle_id,m.model_name,
  b.brand_name, v.reg_plate,v.year,v.class,v.vin,v.transmissions,v.engine,v.is_deleted as vehicles_is_deleted,
  o.issue_date,o.due_date,o.notes,o.status,o.is_deleted as order_is_deleted,GROUP_CONCAT(s.name) as service_name, sum(s.price) AS sum,CONCAT(empl.first_name, " ",empl.last_name) as employee_full_name
  FROM orders_has_services ord
  JOIN orders o ON o.id = ord.orders_id
  JOIN services s ON s.id = ord.services_id
  JOIN vehicles v ON v.id = o.vehicle_id
  JOIN users u ON u.id = v.customer_id
  JOIN users empl ON empl.id = o.users_id
  JOIN addresses a ON a.id = u.addresses_id
  JOIN models m ON m.id = v.models_id
  JOIN brands b ON b.id = m.brand_id
  WHERE  u.id=? 
  AND    o.id =?
  GROUP BY order_id
    `;

  return await pool.query(sql, [+data.id, +data.ordId]);
};
const getOrder = async (data) => {
  const sql = `
    SELECT id, vehicle_id FROM orders  WHERE id=?
    `;

  return await pool.query(sql, [+data.ordId]);
};
const getCreated = async (data) => {
  const sql = `
  SELECT id, issue_date, due_date, notes, status from orders WHERE id=?;
    `;

  return await pool.query(sql, [+data]);
};
const getBy = async (column, value) => {
  const sql = `
  SELECT o.id as order_id , u.id as customer_id,CONCAT(u.first_name, " ",u.last_name) AS fullname, u.email,u.phone_number,u.role, u.is_deleted as user_is_deleted,
  a.id as addresses_id,a.street_address,a.city,a.country,a.postal_code ,v.id as vehicle_id,m.model_name,
  b.brand_name, v.reg_plate,v.year,v.class,v.vin,v.transmissions,v.engine,v.is_deleted as vehicles_is_deleted,
  o.issue_date,o.due_date,o.notes,o.status,o.is_deleted as order_is_deleted,GROUP_CONCAT(s.name) as service_name, sum(s.price) AS sum,CONCAT(empl.first_name, " ",empl.last_name) as employee_full_name, u.avatar
  FROM orders_has_services ord
  JOIN orders o ON o.id = ord.orders_id
  JOIN services s ON s.id = ord.services_id
  JOIN vehicles v ON v.id = o.vehicle_id
  JOIN users u ON u.id = v.customer_id
  JOIN users empl ON empl.id = o.users_id
  JOIN addresses a ON a.id = u.addresses_id
  JOIN models m ON m.id = v.models_id
  JOIN brands b ON b.id = m.brand_id
  WHERE   ${column} =? AND o.is_deleted=0
  GROUP BY order_id
    `;

  return await pool.query(sql, [+value]);
};
const searchByQueries = async (column, id, data, orderQueries) => {
  const direction = data.sort || 'ASC';
  const startIndex = data.page ? (data.page - 1) * data.limit : 0;
  const sql1 = `
  SELECT o.id as order_id , u.id as customer_id,CONCAT(u.first_name, " ",u.last_name) AS fullname, u.email,u.phone_number,u.role, u.is_deleted as user_is_deleted,
  a.id as addresses_id,a.street_address,a.city,a.country,a.postal_code ,v.id as vehicle_id,m.model_name,
  b.brand_name, v.reg_plate,v.year,v.class,v.vin,v.transmissions,v.engine,v.is_deleted as vehicles_is_deleted,
  o.issue_date,o.due_date,o.notes,o.status,o.is_deleted as order_is_deleted,GROUP_CONCAT(s.name) as service_name, sum(s.price) AS sum,CONCAT(empl.first_name, " ",empl.last_name) as employee_full_name
  FROM orders_has_services ord
  JOIN orders o ON o.id = ord.orders_id
  JOIN services s ON s.id = ord.services_id
  JOIN vehicles v ON v.id = o.vehicle_id
  JOIN users u ON u.id = v.customer_id
  JOIN users empl ON empl.id = o.users_id
  JOIN addresses a ON a.id = u.addresses_id
  JOIN models m ON m.id = v.models_id
  JOIN brands b ON b.id = m.brand_id
  `;

  const sql2Arr = queryFuncSql(orderQueries, 'o');
  const sql2 = checkLengthSql(sql2Arr);

  const sql3 = ` ORDER BY o.due_date ${direction}
  LIMIT ${startIndex} `;
  const sql4 = `GROUP BY order_id`;
  const sql5 = `WHERE  ${column} =${+id} AND `;
  const sqlQueries = sql1.concat(sql5, sql2).slice(0, -4);
  const sql = sqlQueries + sql4 + sql3;

  return await pool.query(sql);
};
const getServices = async (ordId, data) => {
  const sql = `
  SELECT  ord.orders_id, ord.services_id,s.name
  FROM orders_has_services as ord
  JOIN  services s ON s.id = ord.services_id
  WHERE orders_id=? AND  s.id =?
  Group by  s.id
  `;
  const service = await pool.query(sql, [+ordId, +data]);
  return service;
};

const deleteOrder = async (value) => {
  const sql = `UPDATE orders SET is_deleted = 1 WHERE id = ?;
  `;
  return await pool.query(sql, [+value]);
};

const update = async (data, ordId) => {
  const sql = `UPDATE orders SET status = ? WHERE id = ?`;
  return await pool.query(sql, [data.status, +ordId]);
};
const create = async (data, dataBody) => {
  const sql = `INSERT INTO orders (issue_date, due_date, vehicle_id, users_id, notes, status) VALUES (?,?,?,?,?,?)`;
  return await pool.query(sql, [
    dataBody.issue_date,
    dataBody.due_date,
    +data.id,
    +data.user,
    dataBody.notes,
    dataBody.status,
  ]);
};
const addService = async (ordId, data) => {
  const sql = `INSERT INTO orders_has_services (orders_id, services_id) VALUES (?,?)`;
  return await pool.query(sql, [+ordId, +data]);
};

export default {
  getAll,
  getBy,
  getByOrder,
  deleteOrder,
  update,
  addService,
  getServices,
  create,
  getCreated,
  searchByQueries,
  getOrder,
};
