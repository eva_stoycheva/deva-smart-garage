import { sendRegistrationEmail } from '../common/send-mails.js';
import { filterQueries } from '../common/helpers.js';
import serviceErrors from './service-errors.js';
import bcrypt from 'bcrypt';

const signInUser = (usersData) => {
  return async (email, password) => {
    const user = await usersData.getBy('u.email', email);
    if (!user[0] || !(await bcrypt.compare(password, user[0].password))) {
      return {
        error: serviceErrors.INVALID_SIGNIN,
        user: null,
      };
    }
    return {
      error: null,
      user: user[0],
    };
  };
};

const getAllUsers = (usersData) => {
  return async (data, user) => {
    const filter = Object.keys(data).reduce((acc, query) => {
      if (query !== 'limit' && query !== 'sort' && query !== 'page') {
        acc.push(query);
      }
      return acc;
    }, []);

    if (filter.length) {
      const userQueries = filterQueries(
          data,
          'first_name',
          'last_name',
          'email',
          'phone_number'
      );
      const vehicleQueries = filterQueries(
          data,
          'year',
          'reg_plate',
          'class',
          'vin',
          'transmissions',
          'engine'
      );
      const brandQueries = filterQueries(data, 'brand_name');
      const modelQueries = filterQueries(data, 'model_name');
      const orderQueries = filterQueries(data, 'issue_date', 'due_date');

      const result = await usersData.searchByQueries(
          userQueries,
          vehicleQueries,
          brandQueries,
          modelQueries,
          orderQueries,
          data
      );

      if (!result[0]) {
        return { error: serviceErrors.NO_MATCHES, users: null };
      }
      return { error: null, users: result };
    }
    const users = await usersData.getAll(data, user);
    if (!users[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        users: null,
      };
    }

    return { error: null, users: users };
  };
};

const getUserById = (usersData) => {
  return async (id) => {
    const userById = await usersData.getBy('u.id', +id);

    if (!userById[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        vehicle: null,
      };
    }

    return { error: null, user: userById };
  };
};

const getUserByEmail = (usersData) => {
  return async (data) => {
    const userByMail = await usersData.getBy('u.email', data);
    if (!userByMail[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        vehicle: null,
      };
    }

    return { error: null, user: userByMail[0] };
  };
};
const createUser = (usersData) => {
  return async (data, avatar, user) => {
    const existingUser = await usersData.getBy('u.email', data.email);

    if (existingUser[0]) {
      return {
        error: serviceErrors.DUPLICATED_RECORD,
        user: null,
      };
    }
    const address = await usersData.createAddress(data, user);
    data = {
      ...data,
      addresses_id: address[0].id,
    };

    const pass = 'deva' + Math.round(Math.random() * 10000);
    data.password = await bcrypt.hash(pass, 10);
    const newUser = await usersData.create(data, avatar);
    sendRegistrationEmail(data.email, data.first_name, pass);
    return {
      error: null,
      user: newUser,
    };
  };
};

const deleteUser = (usersData) => {
  return async (id) => {
    const userDelete = await usersData.getBy('u.id', +id);

    if (!userDelete[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }
    const deletedUser = await usersData.deleted(id);

    return {
      error: null,
      user: deletedUser,
    };
  };
};

const updateUser = (usersData) => {
  return async (data, id, avatar) => {
    const userToUpdate = await usersData.getBy('u.id', +id);

    if (!userToUpdate[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    await usersData.update(data, id, avatar);
    const updated = await usersData.getBy('u.id', +id);

    return {
      error: null,
      user: updated,
    };
  };
};

const updatePassword = (usersData, tokensData) => {
  return async (id, password, token) => {
    const userToUpdate = await usersData.getBy('u.id', +id);
    const hashedPassword = await bcrypt.hash(password, 10);

    if (!userToUpdate[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }
    await tokensData.updateToken(token);
    await usersData.updateUserPassword(+id, hashedPassword);

    return {
      UserError: null,
      message: 'Password updated successfully',
    };
  };
};
const checkDeletedUser = (userData) => {
  return async (email) => {
    const user = await userData.checkDeleted(email);

    if (user[0]) {
      return {
        error: serviceErrors.INVALID_ACCOUNT,
        user: null,
      };
    }

    return { error: null, user: null };
  };
};

export default {
  checkDeletedUser,
  getUserByEmail,
  updatePassword,
  getAllUsers,
  getUserById,
  createUser,
  signInUser,
  deleteUser,
  updateUser,
};
