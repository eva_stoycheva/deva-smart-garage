import serviceErrors from './service-errors.js';
import { filterQueries } from '../common/helpers.js';

const getAllServices = (servicesData) => {
  return async (data) => {
    const filter = Object.keys(data).reduce((acc, query, index, arr) => {
      if (query !== 'limit' && query !== 'sort' && query !== 'page') {
        acc.push(query);
      }
      return acc;
    }, []);

    if (filter.length) {
      const serviceQueries = filterQueries(data, 'name', 'price');
      const result = await servicesData.searchByQueries(data, serviceQueries);
      if (!result[0]) {
        return { error: serviceErrors.NO_MATCHES, service: null };
      }
      return { error: null, services: result[0] };
    }
    const services = await servicesData.getAll(data);
    if (!services[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        services: null,
      };
    }

    return { error: null, services: services };
  };
};

const deleteService = (servicesData) => {
  return async (data) => {
    const services = await servicesData.getBy('id', +data.id);
    if (!services[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        services: null,
      };
    }
    const del = await servicesData.deleteService(data.id);
    const deleted = { is_deleted: 1 };
    const removed = await servicesData.getBy('id', +data.id, deleted);
    return { error: null, services: removed };
  };
};

const createService = (servicesData) => {
  return async (data) => {
    const services = await servicesData.getBy('name', data.name);
    if (services[0]) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        services: null,
      };
    }
    const create = await servicesData.create(data);
    const created = await servicesData.getBy('id', +create.insertId);
    return { error: null, services: created[0] };
  };
};
const updateService = (servicesData) => {
  return async (data, dataBody) => {
    const services = await servicesData.getBy('id', +data.id);
    if (!services[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        services: null,
      };
    }
    const update = await servicesData.update(dataBody, data);
    const updated = await servicesData.getBy('id', +data.id);
    return { error: null, services: updated };
  };
};
export default {
  getAllServices,
  deleteService,
  createService,
  updateService,
};
