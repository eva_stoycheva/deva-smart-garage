import { sendResetPassWordEmail } from '../common/send-mails.js';
import serviceErrors from './service-errors.js';
import { v4 as uuidv4 } from 'uuid';

const create = (tokensData) => {
  return async (data) => {
    const token = uuidv4();

    await tokensData.save(token, data.id);
    sendResetPassWordEmail(data.email, token, data.first_name, data.last_name);
    return token;
  };
};

const getToken = (tokensData) => {
  return async (data) => {
    const foundToken = await tokensData.checkExistingToken(data);
    if (!foundToken) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        message: 'Token not found',
      };
    }

    if (foundToken.expire_time.valueOf() < new Date().valueOf()) {
      return {
        error: serviceErrors.OPERATION_NOT_ALLOWED,
        message: 'The link has expired',
      };
    }
    return {
      tokenResult: foundToken,
      error: null,
    };
  };
};

export default {
  getToken,
  create,
};
