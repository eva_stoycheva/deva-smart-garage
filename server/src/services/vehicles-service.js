import { filterQueries } from '../common/helpers.js';
import serviceErrors from './service-errors.js';

const getAllVehicles = (vehicleData) => {
  return async (data, user) => {
    const filter = Object.keys(data).reduce((acc, query) => {
      if (query !== 'limit' && query !== 'sort' && query !== 'page') {
        acc.push(query);
      }
      return acc;
    }, []);

    if (filter.length) {
      const userQueries = filterQueries(
          data,
          'first_name',
          'last_name',
          'email',
          'phone_number'
      );
      const vehicleQueries = filterQueries(
          data,
          'year',
          'reg_plate',
          'class',
          'vin',
          'transmissions',
          'engine'
      );
      const brandQueries = filterQueries(data, 'brand_name');
      const modelQueries = filterQueries(data, 'model_name');
      const orderQueries = filterQueries(data, 'issue_date', 'due_date');

      const result = await vehicleData.searchByQueries(
          userQueries,
          vehicleQueries,
          brandQueries,
          modelQueries,
          orderQueries,
          data
      );

      if (!result[0]) {
        return { error: serviceErrors.NO_MATCHES, vehicles: null };
      }
      return { error: null, vehicles: result };
    }
    const vehicles = await vehicleData.getAll(data, user);
    if (!vehicles[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        vehicles: null,
      };
    }

    return { error: null, vehicles: vehicles };
  };
};

const getVehicleById = (vehicleData) => {
  return async (id, user) => {
    const vehicle = await vehicleData.getById(id, user);
    if (!vehicle[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        vehicle: null,
      };
    }

    return { error: null, vehicle: vehicle };
  };
};

const getVehiclesByUserId = (vehicleData) => {
  return async (id, user) => {
    const vehicle = await vehicleData.getBy('v.customer_id', id);
    if (!vehicle[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        vehicle: null,
      };
    }

    return { error: null, vehicle: vehicle };
  };
};

const updateVehicle = (vehicleData) => {
  return async (id, data, user) => {
    const vehicle = await vehicleData.getById(id, user);
    if (!vehicle[0] || vehicle[0].is_deleted === 1) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        vehicle: null,
      };
    }
    // check if model and brand combination already exist
    const model = await vehicleData.searchByModelAndBrand(
        data.model,
        data.brand
    );

    if (!model[0]) {
      const newModel = await vehicleData.createModel(data, user);
      await vehicleData.update(id, data, newModel.insertId, user);
      const updated = await vehicleData.getById(id, user);
      return { error: null, vehicle: updated };
    }
    await vehicleData.update(id, data, model[0].id, user);
    const updated = await vehicleData.getById(id, user);

    return { error: null, vehicle: updated };
  };
};

const deleteVehicle = (vehicleData) => {
  return async (id, user) => {
    const vehicle = await vehicleData.getById(id, user);
    if (!vehicle[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        vehicle: null,
      };
    }

    const deleted = await vehicleData.deleted(id, user);
    return { error: null, vehicle: deleted };
  };
};

const createVehicle = (vehicleData) => {
  return async (data, user) => {
    // check if vin already exist
    const vehicleVin = await vehicleData.getByVin(data.vin);
    if (vehicleVin[0]) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        vehicle: null,
      };
    }

    // check if model and brand combination already exist
    const model = await vehicleData.searchByModelAndBrand(
        data.model,
        data.brand
    );
    if (!model[0]) {
      const newModel = await vehicleData.createModel(data, user);
      const newVehicle = await vehicleData.create(
          data,
          newModel.insertId,
          user
      );
      const created = await vehicleData.getById(newVehicle.insertId, user);
      return { error: null, vehicle: created };
    }
    const newVehicle = await vehicleData.create(data, model[0].id, user);
    const created = await vehicleData.getById(newVehicle.insertId, user);
    return { error: null, vehicle: created };
  };
};

export default {
  getVehiclesByUserId,
  getVehicleById,
  getAllVehicles,
  updateVehicle,
  deleteVehicle,
  createVehicle,
};
