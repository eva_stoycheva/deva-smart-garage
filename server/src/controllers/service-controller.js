import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import createServiceSchema from '../validators/create-service-schema.js';
import updateServiceSchema from '../validators/update-service-schema.js';
import loggerUserGuard from '../middleware/loggerUserGuard.js';
import servicesService from '../services/services-service.js';
import serviceErrors from '../services/service-errors.js';
import validateBody from '../middleware/validate-body.js';
import { userRole } from '../common/user-role.js';
import servicesData from '../data/services.js';
import express from 'express';

const servicesController = express.Router();

servicesController
    .get('/services', authMiddleware, loggerUserGuard, async (req, res) => {
      const data = req.query;
      const { error, services } = await servicesService.getAllServices(servicesData)(data);
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Services  not found!', error: error });
      } else if (error === serviceErrors.NO_MATCHES) {
        res.status(404).send({ message: 'Services with such queries not found!', error: error });
      } else {
        res.status(200).send(services);
      }
    })
    .post('/services', authMiddleware, loggerUserGuard, roleMiddleware(userRole.Employee), validateBody('service', createServiceSchema), async (req, res) => {
      const dataBody = req.body;
      const { error, services } = await servicesService.createService(servicesData)(dataBody);
      if (error === serviceErrors.DUPLICATE_RECORD) {
        res.status(409).send({ message: 'Service already added!', error: error });
      } else {
        res.status(200).send(services);
      }
    })
    .delete('/services/:id', authMiddleware, loggerUserGuard, roleMiddleware(userRole.Employee), async (req, res) => {
      const data = req.params;
      const { error, services } = await servicesService.deleteService(servicesData)(data);
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Service  not found!', error: error });
      } else {
        res.status(200).send(services);
      }
    })
    .put('/services/:id', authMiddleware, loggerUserGuard, roleMiddleware(userRole.Employee), validateBody('service', updateServiceSchema), async (req, res) => {
      const data = req.params;
      const dataBody = req.body;
      const { error, services } = await servicesService.updateService(servicesData)(data, dataBody );
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Service  not found!', error: error });
      } else {
        res.status(200).send(services);
      }
    });

export default servicesController;
