import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import updateUserScheme from '../validators/update-user-scheme.js';
import createUserScheme from '../validators/create-user-scheme.js';
import loggerUserGuard from '../middleware/loggerUserGuard.js';
import serviceErrors from '../services/service-errors.js';
import validateBody from '../middleware/validate-body.js';
import usersService from '../services/users-service.js';
import { userRole } from '../common/user-role.js';
import usersData from '../data/users.js';
import express from 'express';
import bcrypt from 'bcrypt';
import multer from 'multer';
import path from 'path';

const usersController = express.Router();

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'avatars');
  },
  filename(req, file, cb) {
    const filename = Date.now() + path.extname(file.originalname);
    cb(null, filename);
  },
});

const upload = multer({ storage });

usersController
    .get('/users', authMiddleware, loggerUserGuard, roleMiddleware(userRole.Employee), async (req, res) => {
      const data = req.query;
      const { error, users } = await usersService.getAllUsers(usersData)(
          data
      );
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(409).send({ message: 'Users not found!', error: error });
      } else if (error === serviceErrors.NO_MATCHES) {
        res.status(409).send({ message: 'Users with such queries not found!', error: error });
      } else {
        res.status(200).send(users);
      }
    })
    .get('/users/:id', authMiddleware, loggerUserGuard, async (req, res) => {
      const { id } = req.params;
      const { error, user } = await usersService.getUserById(usersData)(id);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(409).send({ message: 'User does not exist!', error: error });
      } else {
        res.status(201).send(user);
      }
    })
    .post('/users', upload.single('avatar'), authMiddleware, loggerUserGuard, roleMiddleware(userRole.Employee),
        validateBody('users', createUserScheme), async (req, res) => {
          const data = req.body;
          const avatar = req.file || '';

          const { error, user } = await usersService.createUser(usersData)(data, avatar);
          if (error === serviceErrors.DUPLICATED_RECORD) {
            res.status(409).send({ message: 'User already exists', error: error });
          } else {
            res.status(201).send(user);
          }
        }
    )
    .put('/users/:id', upload.single('avatar'), authMiddleware, loggerUserGuard,
        validateBody('users', updateUserScheme),

        async (req, res) => {
          const { id } = req.params;
          const data = req.body;
          const avatar = req.file;
          const { error, user } = await usersService.updateUser(usersData)(data, id, avatar);

          if (error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(409).send({ message: 'User does not exist!', error: error });
          } else {
            res.status(201).send(user);
          }
        }
    )
    .delete('/users/:id', authMiddleware, loggerUserGuard, roleMiddleware(userRole.Employee), async (req, res) => {
      const { id } = req.params;
      const { error, user } = await usersService.deleteUser(usersData)(id);
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(409).send({ message: 'User does not exist!', error: error });
      } else {
        res.status(201).send(user);
      }
    }
    )
    .put('/users/avatars', authMiddleware, loggerUserGuard, roleMiddleware(userRole.Employee), upload.single('avatar'), async (req, res) => {
      const info = req.file;
      res.json({ message: 'Avatar uploaded!', info: info });
    });

export default usersController;
