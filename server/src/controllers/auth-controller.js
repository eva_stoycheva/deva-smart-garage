import resetPasswordValidator from '../validators/reset-password-schema.js';
import userLoginScheme from '../validators/user-login-scheme.js';
import loggerUserGuard from '../middleware/loggerUserGuard.js';
import { authMiddleware } from '../auth/auth-middleware.js';
import serviceErrors from './../services/service-errors.js';
import validateBody from '../middleware/validate-body.js';
import tokenService from '../services/token-service.js';
import userService from '../services/users-service.js';
import createToken from '../auth/create-token.js';
import tokensData from './../data/tokens.js';
import usersData from './../data/users.js';
import express from 'express';

const authController = express.Router();

authController
    .post('/login', validateBody('login', userLoginScheme), async (req, res) => {
      const { email, password } = req.body;
      if (email) {
        const { error, user } = await userService.checkDeletedUser(usersData)(email);

        if (error === serviceErrors.INVALID_ACCOUNT) {
          res.status(400).send({ message: 'Invalid account!' });
        }
      }
      const { error, user } = await userService.signInUser(usersData)(email, password);

      if (error === serviceErrors.INVALID_SIGNIN) {
        res.status(400).send({ message: 'Invalid username/password!' });
      } else {
        const payload = {
          sub: user.id,
          email: user.email,
          role: user.role,
        };
        const token = createToken(payload);
        res.status(200).send({
          token: token,
        });
      }
    })
    .post('/forgot', async (req, res) => {
      const { error, user } = await userService.getUserByEmail(usersData)(req.body.email);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'User is not found' });
      } else {
        const token = await tokenService.create(tokensData)(user);

        res.status(200).send({ token: token });
      }
    })
    .delete('/logout', authMiddleware, loggerUserGuard, async (req, res) => {
      const token = req.headers.authorization.replace('Bearer ', '');

      await usersData.logoutUser(token);
      res.send({ message: 'Successfully logged out!' });
    })
    .post('/reset', validateBody('login', resetPasswordValidator), async (req, res) => {
      const { password, token } = req.body;
      const { tokenResult, error } = await tokenService.getToken(tokensData)(token);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).json({
          message: 'Token not found',
        });
      }

      if (error === serviceErrors.OPERATION_NOT_ALLOWED) {
        res.status(405).send({ message: 'The link is not valid anymore', error: error });
      }
      const { UserError, message } = await userService.updatePassword(usersData, tokensData)(tokenResult.user_reset_id, password, token);

      if (UserError === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'User not found' });
      } else {
        res.status(200).send({ message: message });
      }
    },
    );
export default authController;
