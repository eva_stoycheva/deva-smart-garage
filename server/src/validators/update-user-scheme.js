import {
  validateString,
  validateLength,
  isValid,
  emailValidator,
  phoneValidator,
  postCodeValidator,
} from '../common/validators.js';

export default {
  first_name: (value) =>
    isValid(value) && validateString(value) && validateLength(value, 2, 20),

  last_name: (value) =>
    isValid(value) && validateString(value) && validateLength(value, 2, 20),

  email: (value) =>
    isValid(value) &&
    validateString(value) &&
    validateLength(value, 3, 100) &&
    emailValidator(value),

  phone_number: (value) =>
    isValid(value) && validateString(value) && phoneValidator(value),

  street_address: (value) =>
    isValid(value) && validateString(value) && validateLength(value, 3, 45),

  city: (value) =>
    isValid(value) && validateString(value) && validateLength(value, 3, 45),

  country: (value) =>
    isValid(value) && validateString(value) && validateLength(value, 3, 45),

  postal_code: (value) =>
    isValid(value) && validateString(value) && postCodeValidator(value),

  avatar: (value) => isValid(value) && validateString(value),
};
