import {
  validateString,
  validateLength,
  
  isValid,
} from '../common/validators.js';

export default {
  name: (value) => validateString(value) && validateLength(value, 3, 45),

  price: (value) =>isValid(value),
};