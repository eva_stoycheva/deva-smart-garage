import {
  validateString,
  validateLength,
  isValid,
} from '../common/validators.js';

export default {
  password: (value) =>
    isValid(value) && validateString(value) && validateLength(value, 8, 20),
};
