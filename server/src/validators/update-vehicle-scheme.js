import {
  licensePlateValidator,
  yearValidator,
  classValidator,
  transmissionsValidator,
  engineValidator,
  validateLength,
  isValidBrand,
  isValid,
  validateString,
} from '../common/validators.js';

export default {
  reg_plate: (value) =>
    isValid(value) && validateString(value) && licensePlateValidator(value),

  year: (value) => isValid(value) && yearValidator(value),

  class: (value) => isValid(value) && classValidator(value),

  transmissions: (value) => isValid(value) && transmissionsValidator(value),

  engine: (value) => isValid(value) && engineValidator(value),

  model: (value) =>
    isValid(value) && validateString(value) && validateLength(value, 1, 20),

  brand: (value) => isValidBrand(value),
};
