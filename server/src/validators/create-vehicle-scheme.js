import {
  vinValidator,
  licensePlateValidator,
  yearValidator,
  classValidator,
  transmissionsValidator,
  engineValidator,
  validateLength,
  isValid,
  validateString,
  isValidBrand,
  validateNumber,
} from '../common/validators.js';

export default {
  reg_plate: (value) =>
    isValid(value) && validateString(value) && licensePlateValidator(value),

  year: (value) =>
    isValid(value) && validateString(value) && yearValidator(value),

  class: (value) => isValid(value) && classValidator(value),

  vin: (value) =>
    isValid(value) && validateString(value) && vinValidator(value),

  transmissions: (value) => isValid(value) && transmissionsValidator(value),

  engine: (value) => isValid(value) && engineValidator(value),

  model: (value) =>
    isValid(value) && validateString(value) && validateLength(value, 1, 20),

  brand: (value) => isValidBrand(value),

  customer_id: (value) => validateNumber(value),
};
