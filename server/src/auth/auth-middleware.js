import passport from 'passport';

const authMiddleware = passport.authenticate('jwt', { session: false });

const roleMiddleware = (roleName) => {
  return async (req, res, next) => {
    if ( req.user && req.user.role === roleName ) {
      await next();
    } else {
      res.status(403).send({
        message: 'Resource is forbidden.',
      });
    }
  };
};

export {
  authMiddleware,
  roleMiddleware,
};
