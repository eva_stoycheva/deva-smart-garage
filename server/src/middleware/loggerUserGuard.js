import { tokenExist } from '../data/tokens.js';

export default async (req, res, next) => {
  const token = req.headers.authorization.replace('Bearer ', '');
  const userToken = await tokenExist(token);

  if (userToken) {
    return res.status(401).send({ message: 'You\'re not logged in!' });
  }

  await next();
};
