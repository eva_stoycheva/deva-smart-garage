import { validationSchemaReset } from "../../../common/validators";
import { useStylesResetPassword } from "./ResetPasswordStyles";
import { ToastContainer, toast, Bounce } from 'react-toastify'
import { Formik, Form, Field, ErrorMessage } from "formik";
import { Copyright } from "../../Base/Copyright/Copyright";
import CssBaseline from "@material-ui/core/CssBaseline";
import { BASE_URL } from "../../../common/constants";
import TextField from "@material-ui/core/TextField";
import 'react-toastify/dist/ReactToastify.css';
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import React from "react";

const ResetPassword = ({ history, location }) => {
  const classes = useStylesResetPassword();
  const token = location.search.substring(4)
  const initialValues = {
    password: "",
    passwordConfirmation: "",
  };

  const resetPassword = (values, { setSubmitting }) => {

    const password = values.password;
    const data = { password, token }
    fetch(`${BASE_URL}/reset`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",

      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.error) {
          toast.error(`${error.message}`, {
            position: toast.POSITION.TOP_CENTER,
            transition: Bounce,
            autoClose: false,
            onOpen: () => setSubmitting(true),
            onClose: () => history.push('/login')
          })
        } else {
          toast.success('You successfully reset your password!', {
            position: toast.POSITION.TOP_CENTER,
            transition: Bounce,
            autoClose: false,
            onOpen: () => setSubmitting(true),
            onClose: () => history.push('/login')
          })
        }
      })
      .catch((error) => {
        toast.error(`${error.message}`, {
          position: toast.POSITION.TOP_CENTER,
          transition: Bounce,
          autoClose: false,
          onOpen: () => setSubmitting(true),
          onClose: () => setSubmitting(true)
        })
      });
  };

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={21}>
        <ToastContainer />
        <div className={classes.paper}>
          <h2 style={{ fontSize: "18px" }}>RESET YOUR PASSWORD</h2>
          <Box mt={1}>
            <p style={{ fontSize: "18px" }}>
              *Please fill your e-mail address and you will receive a link to
              reset your password!
            </p>{" "}
          </Box>
          <Formik
            initialValues={initialValues}
            onSubmit={resetPassword}
            validationSchema={validationSchemaReset}
          >
            {(props) => (
              <Form className={classes.form}>
                <Field style={{ paddingBottom: "20px" }}
                  as={TextField}
                  variant="outlined"
                  name="password"
                  placeholder="Enter a new password"
                  required
                  fullWidth
                  id="password"
                  type="password"
                  label="Enter a new password"
                  autoFocus
                  helperText={<ErrorMessage name="password" />}
                  error={props.errors.password ? true : false}
                  onChange={props.handleChange}
                />
                <Field
                  as={TextField}
                  variant="outlined"
                  name="passwordConfirmation"
                  placeholder="Confirm new password"
                  required
                  fullWidth
                  id="passwordConfirmation"
                  label="Confirm password"
                  autoFocus
                  type="password"
                  helperText={<ErrorMessage name="passwordConfirmation" />}
                  error={props.errors.passwordConfirmation ? true : false}
                  onChange={props.handleChange}
                />
                <Box mt={1}>
                  <Button style={{ padding: "12px 28px", fontSize: "15px" }}
                    type="submit"
                    variant="outlined"
                    color="secondary"
                    className={classes.button}
                    disabled={props.isSubmitting}
                  >
                    SEND
                  </Button>

                </Box>
              </Form>
            )}
          </Formik>
          <Copyright />
        </div>
      </Grid>
    </Grid>
  );
};

export default ResetPassword;
