import React, { useState, useEffect } from "react";
import { BASE_URL } from "../../../common/constants";
import MaterialTable from "material-table";
import { tableIcons } from "../../../common/material-table-icons";

const ServicesUser = () => {
  const columns = [
    { field: "name", title: "Service", editable: "onAdd" },
    {
      field: "price",
      title: "Price €",
      type: "number",
    },
  ];
  const [rows, setRows] = useState([]);
  const [services, setServices] = useState([]);
  const [isClicked, setIsClicked] = useState(false);

  useEffect(() => {
    setRows(services);
  }, [services]);

  const getAllServices = () => {
    fetch(`${BASE_URL}/services`, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }

        setServices(result);
      })
      .catch((error) => console.log(error.message));
  };

  useEffect(() => {
    getAllServices();
  }, [isClicked]);

  return (
    <div>
      <MaterialTable
        icons={tableIcons}
        title="Services"
        columns={columns}
        data={rows}
        options={{
          filtering: true,
          addRowPosition: "first",
          tableLayout: "auto",
          padding: "dense",
          pageSize: 10,
          doubleHorizontalScroll: false,
          width: 16,
          headerStyle: {
            width: 16,
            whiteSpace: "nowrap",
            textAlign: "left",
            flexDirection: "row",
            overflow: "hidden",
            textOverflow: "ellipsis",
            paddingLeft: 5,
            paddingRight: 5,
            backgroundColor: "#f50057",
            color: "whitesmoke",
            fontWeight: "bold",
            fontSize: 16,
            alignContent: "center",
          },
          rowStyle: { backgroundColor: "#E5E5E5", width: 56 },
        }}
      />
    </div>
  );
};
export default ServicesUser;
