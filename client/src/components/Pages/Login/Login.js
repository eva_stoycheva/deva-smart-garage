import { validationSchemaLogin } from "../../../common/validators";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import { ToastContainer, toast, Bounce } from 'react-toastify'
import { Formik, Form, Field, ErrorMessage } from "formik";
import { AuthContext } from "../../../Context/AuthContext";
import { Copyright} from '../../Base/Copyright/Copyright';
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import { BASE_URL } from "../../../common/constants";
import TextField from "@material-ui/core/TextField";
import Container from "@material-ui/core/Container";
import Checkbox from "@material-ui/core/Checkbox";
import { useStylesLogin } from "./LoginStyles";
import 'react-toastify/dist/ReactToastify.css';
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import React, { useContext } from "react";
import Box from "@material-ui/core/Box";
import decode from "jwt-decode";

const Login = ({ history }) => {
  const classes = useStylesLogin();
  const auth = useContext(AuthContext);
  const initialValues = {
    email: "",
    password: ""
  }

  const login = (values, { setSubmitting, resetForm }) => {

    fetch(`${BASE_URL}/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(values),
    })
      .then((res) => res.json())
      .then(({ token }) => {
        const user = decode(token);
        localStorage.setItem("token", token);
        auth.setAuthState({ user, isLoggedIn: true, role: user.role })

        if (user.role === 'employee'){
          toast.success(`You successfully logged in!`, {
            position: toast.POSITION.TOP_CENTER,
            transition: Bounce,
            autoClose: false,
            onOpen: () => setSubmitting(true),
            onClose: () => history.replace('/dashboard')
          })
        }else if (user.role === 'customer'){
          toast.success(`You successfully logged in!`, {
            position: toast.POSITION.TOP_CENTER,
            transition: Bounce,
            autoClose: false,
            onOpen: () => setSubmitting(true),
            onClose: () => history.replace('/profile')
          })
        }
      })
      .catch((error) => {
        toast.error(`${error.message}`, {
          position: toast.POSITION.TOP_CENTER,
          transition: Bounce,
          autoClose: false,
          onOpen: () => setSubmitting(true),
          onClose: () => {
            resetForm(true);
            history.replace('/login')
           } 
        })
      })
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <ToastContainer />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <Formik
          initialValues={initialValues}
          onSubmit={login}
          validationSchema={validationSchemaLogin}
        >
          {(props ) => (
            <Form className={classes.form}>
              <Field
                as={TextField}
                variant="outlined"
                name="email"
                placeholder="Enter email"
                margin="normal"
                label="Email Address"
                autoComplete="email"
                required
                fullWidth
                id="email"
                helperText={<ErrorMessage name="email" />}
                error={props.errors.email ? true : false} 
                onChange={props.handleChange}
              />
              <Field
                as={TextField}
                variant="outlined"
                name="password"
                placeholder="Enter password"
                margin="normal"
                required
                label="Password"
                fullWidth
                type="password"
                id="password"
                autoComplete="current-password"
                helperText={<ErrorMessage name="password" />}
                error={props.errors.password ? true : false}
                onChange={props.handleChange}
              />
              <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Remember me"
              />
              <Button
                type="submit"
                fullWidth
                variant="outlined"
                color="secondary"
                className={classes.submit}
                disabled={props.isSubmitting}
              >
                {props.isSubmitting ? 'LOADING' : 'SIGN IN'}
              </Button>
            </Form>
          )}
        </Formik>
        <Grid container>
          <Grid item xs>
            <Link href="/forgot" variant="body2">
              Forgot password?
                  </Link>
          </Grid>
        </Grid>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
};

export default Login;
