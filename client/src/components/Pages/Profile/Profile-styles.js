import { makeStyles } from "@material-ui/core/styles";

export const useStylesProfile = makeStyles((theme) => ({
  root: {
    height: "100%",
        backgroundImage: `url('http://localhost:3000/light.gif')`,
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  icon: {
    marginRight: theme.spacing(2)
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6)
  },
  heroButtons: {
    marginTop: theme.spacing(4)
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8)
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    padding:"20px"
  },
  cardMedia: {
    paddingTop: "56.25%" ,
  },
  cardContent: {
    flexGrow: 1
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  profileImage: {
    width: "50%",
    borderRadius: "100%"
  },
  content: {
    justifyContent: "left",
    textAlign: "left"
  },
  details: {
    marginTop: "-20px"
  },
  chip: {
    padding: "0 5px",
    margin: "2px 5px"
  },
}));

