import React, { useState, useEffect, useContext } from "react";
import { AuthContext } from "../../../Context/AuthContext";
import CardContent from "@material-ui/core/CardContent";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import { BASE_URL } from "../../../common/constants";
import CardMedia from "@material-ui/core/CardMedia";
import Container from "@material-ui/core/Container";
import { useStylesProfile } from "./Profile-styles";
import { Button, List } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import Footer from "../../Base/Footer/Footer";
import Paper from "@material-ui/core/Paper";
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import Chip from "@material-ui/core/Chip";

const Profile = ({ history }) => {
  const classes = useStylesProfile();
  const [userOrders, setUserOrders] = useState([]);
  const { user } = useContext(AuthContext);
  const [userId, setUserId] = useState(user.sub);
  const [userInfo, setUserInfo] = useState([]);
  const [currency, setCurrency] = useState(true);
  const [currencyID, setCurrencyID] = useState(-1);
  const [fromCurrency, setFromCurrency] = useState();
  const [toCurrency, setToCurrency] = useState();
  const [exchangeRate, setExchangeRate] = useState(1);

  useEffect(() => {
    fetch('http://api.exchangeratesapi.io/v1/latest?access_key=74e2abd666a0905b7b29700d4812bb72')
      .then(res => res.json())
      .then(data => {
        const firstCurrency = Object.keys(data.rates)[149]
        setFromCurrency(data.base)
        setToCurrency(firstCurrency)
        setExchangeRate(data.rates[firstCurrency])
      })
  }, [])

  useEffect(() => {
    fetch(`${BASE_URL}/user/order`, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setUserOrders(result);
      })
      .catch((error) => {
        console.log(error.message);
      });
  }, []);

  useEffect(() => {
    fetch(`${BASE_URL}/users/${userId}`, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setUserInfo(result);
      });
  }, []);

  return (
    <React.Fragment>
      <Grid container component="main" className={classes.root}>
        <CssBaseline />
        {userInfo.length && (
          <Grid
            item
            key={userInfo[0].id}
            xs={12}
            sm={8}
            md={3}
            component={Paper}
            elevation={6}
            square
          >
            <div className={classes.paper}>
              /
              <div container>
                <img
                  className={classes.profileImage}
                  alt="Avatar"
                  src={userInfo[0].avatar}
                />
                <div className={classes.content}>
                  <Typography component="h5" variant="h4">
                    {userInfo[0].full_name}
                  </Typography>
                  <Divider style={{ padding: 5 }} />
                  <Chip
                    className={classes.chip}
                    color="secondary"
                    label={userInfo[0].role}
                    size="small"
                  />{" "}
                  <Divider style={{ padding: 5 }} />
                  <Grid container justify="space-between">
                    <Grid item>
                      <List>
                        <Typography component="h3" variant="h6">
                          Personal information:
                        </Typography>{" "}
                        <Divider style={{ padding: 5 }} />
                        <Typography style={{ padding: 10 }}>
                          E-mail: {userInfo[0].email}
                        </Typography>{" "}
                        <Divider style={{ padding: 5 }} />
                        <Typography style={{ padding: 10 }}>
                          Phone number: {userInfo[0].phone_number}
                        </Typography>{" "}
                        <Divider style={{ padding: 5 }} />
                        <Typography style={{ padding: 10 }}>
                          Country: {userInfo[0].country}
                        </Typography>{" "}
                        <Divider style={{ padding: 5 }} />
                        <Typography style={{ padding: 10 }}>
                          City: {userInfo[0].city}
                        </Typography>{" "}
                        <Divider style={{ padding: 5 }} />
                        <Typography style={{ padding: 10 }}>
                          Post code: {userInfo[0].postal_code}
                        </Typography>{" "}
                        <Divider style={{ padding: 5 }} />
                        <Typography style={{ padding: 10 }}>
                          Address: {userInfo[0].street_address}
                        </Typography>{" "}
                      </List>
                    </Grid>
                    <Grid item></Grid>
                  </Grid>
                  <Grid
                    container
                    justify="space-between"
                    className={classes.details}
                  ></Grid>
                </div>
              </div>{" "}
            </div>
          </Grid>
        )}
        <Grid item xs={false} sm={10} md={9}>
          <main>
            <Container className={classes.cardGrid} maxWidth="md">
              <Grid container spacing={1}>
                {userOrders.length ? (
                  Object.values(userOrders).map((el) => (
                    <Grid item key={el.id} xs={6} sm={6} md={4}>
                      <Card className={classes.card}>
                        <CardMedia
                          className={classes.cardMedia}
                          image="http://localhost:3000/logo.jpg"
                          title={el.status}
                        />
                        <CardContent className={classes.cardContent}>
                          <Typography gutterBottom variant="h5" component="h2">
                            {el.brand_name}
                          </Typography>
                          <Divider style={{ padding: 2 }} />
                          <Typography gutterBottom variant="h5" component="h2">
                            Model: {el.model_name}
                          </Typography>
                          <Divider />
                          <Typography gutterBottom variant="h5" component="h2">
                            Year: {el.year}
                          </Typography>
                          <Divider />
                          <Typography style={{ padding: 5 }}>
                            Reg.plate: {el.reg_plate}
                          </Typography>
                          <Divider />
                          <Typography style={{ padding: 5 }}>
                            VIN: {el.vin}
                          </Typography>
                          <Divider />
                          <Typography style={{ padding: 5 }}>
                            Class: {el.class}
                          </Typography>
                          <Divider />
                          <Typography style={{ padding: 5 }}>
                            Engine: {el.engine}
                          </Typography>
                          <Divider />
                          <Typography style={{ padding: 5 }}>
                            Transmission: {el.transmissions}
                          </Typography>
                          <Divider />
                          <Typography style={{ padding: 5 }}>
                            Service: {el.service_name}
                          </Typography>
                          <Divider />
                          <Typography style={{ padding: 5 }}>
                            Issue date:{" "}
                            {new Date(el.issue_date).toLocaleDateString()}
                          </Typography>
                          <Divider />
                          <Typography style={{ padding: 5 }}>
                            Due date:{" "}
                            {new Date(el.due_date).toLocaleDateString()}
                          </Typography>
                          <Divider />
                          <Typography style={{ padding: 5 }}>
                            Notes: {el.notes}
                          </Typography>
                          <Chip
                            className={classes.chip}
                            color="secondary"
                            label={el.status}
                            size="small"
                          />{" "}
                          <Divider />
                          <Typography style={{ padding: 5 }}>
                            <p>Total Sum:</p>{" "}
                          </Typography>
                          {!currency && el.order_id === currencyID ? (
                            <Typography style={{ padding: 5 }}>
                              <p>{(el.sum * exchangeRate).toFixed(2)} $</p>
                              <Button
                                onClick={() => {
                                  setCurrency(!currency), setCurrencyID(-1);
                                }}
                                color="secondary"
                                variant="outlined"
                              >
                                EUR
                              </Button>
                            </Typography>
                          ) : (
                            <Typography style={{ padding: 5 }}>
                              <p>{el.sum.toFixed(2)} €</p>
                              <Button
                                onClick={() => {
                                  setCurrency(!currency),
                                    setCurrencyID(el.order_id);
                                }}
                                color="secondary"
                                variant="outlined"
                              >
                                USD
                              </Button>
                            </Typography>
                          )}
                        </CardContent>
                      </Card>
                    </Grid>
                  ))
                ) : ''}
              </Grid>
            </Container>
          </main>
        </Grid>
      </Grid>
      <Footer />
    </React.Fragment>
  );
};

export default Profile;
