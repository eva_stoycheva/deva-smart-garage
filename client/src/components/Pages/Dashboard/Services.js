import React, { useState, useEffect } from "react";
import { BASE_URL } from "../../../common/constants";
import MaterialTable from "material-table";
import { tableIcons } from "../../../common/material-table-icons";


const Services = () => {
  const columns = [
    {
      title: "Service",
      editable: "onAdd",
      field: "name",
      type: "string",
      validate: (rowData) =>
        rowData.name === "" ? "Name  must be have 3 chars" : "",
    },
    {
      field: "price",
      title: "Price €",
      type: "number",
      validate: (rowData) =>
        rowData.price === "" ? "Name cannot be empty" : "",
    },
  ];
  const [rows, setRows] = useState([]);
  const [services, setServices] = useState([]);
  const [isClicked, setIsClicked] = useState(false);

  useEffect(() => {
    setRows(services);
  }, [services]);

  const getAllServices = () => {
    fetch(`${BASE_URL}/services`, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }

        setServices(result);
      })
      .catch((error) => console.log(error.message));
  };

  useEffect(() => {
    getAllServices();
  }, [isClicked]);

  const updatePrice = (id, value) => {
    fetch(`${BASE_URL}/services/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({ price: value }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
       
      })
      .catch((error) => console.log(error.message));
  };

  const deleteService = (id) => {
    fetch(`${BASE_URL}/services/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
       
      })
      .catch((error) => console.log(error.message));
  };
  const createService = (data) => {
    console.log(data)
    fetch(`${BASE_URL}/services`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setRows([result, ...services]);
        
      })
      .catch((error) => console.log(error.message));
  };
  return (
    <div>
      <MaterialTable
        icons={tableIcons}
        title="Services"
        columns={columns}
        data={rows}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                createService(newData);
                resolve();
              }, 1000);
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                updatePrice(oldData.id, newData.price);
                const dataUpdate = [...rows];
                const index = oldData.tableData.id;
                dataUpdate[index] = newData;
                setRows([...dataUpdate]);
                resolve();
              }, 1000);
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                deleteService(oldData.id);
                const dataDelete = [...rows];
                const index = oldData.tableData.id;
                dataDelete.splice(index, 1);
                setRows([...dataDelete]);
                resolve();
              }, 1000);
            }),
        }}
        options={{
          filtering: true,
          addRowPosition: "first",
          tableLayout: "auto",
          padding: "dense",
          pageSize: 10,
          doubleHorizontalScroll: false,
          headerStyle: {
            width: 26,
            whiteSpace: "nowrap",
            textAlign: "left",
            flexDirection: "row",
            overflow: "hidden",
            textOverflow: "ellipsis",
            paddingLeft: 5,
            paddingRight: 5,
            backgroundColor: "#f50057",
            color: "whitesmoke",
            fontWeight: "bold",
            fontSize: 16,
          },
          rowStyle: { backgroundColor: "#E5E5E5" },
        }}
      />
    </div>
  );
};
export default Services;
