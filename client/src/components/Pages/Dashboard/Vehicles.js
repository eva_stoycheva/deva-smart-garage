import DialogContentText from '@material-ui/core/DialogContentText';
import { tableIcons } from "../../../common/material-table-icons";
import { ToastContainer, toast, Bounce } from 'react-toastify'
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { BASE_URL } from "../../../common/constants";
import TextField from '@material-ui/core/TextField';
import React, { useState, useEffect } from "react";
import MenuItem from "@material-ui/core/MenuItem";
import { brands } from '../../../common/brands';
import { Typography } from "@material-ui/core";
import 'react-toastify/dist/ReactToastify.css';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Select from "@material-ui/core/Select";
import MaterialTable from "material-table";

const Vehicles = () => {
  const columns = [
    { title: "Model", field: "model" },
    { title: "Brand", field: "brand", editable: 'onAdd' },
    { title: "Class", field: "class" },
    { title: "Reg.plate", field: "reg_plate" },
    { title: "Year", field: "year" },
    { title: "Vin", field: "vin", editable: 'onAdd' },
    { title: "Transmissions", field: "transmissions", lookup: { auto: 'auto', manual: 'manual' }, },
    { title: "Engine", field: "engine", lookup: { petrol: 'petrol', diesel: 'diesel', hybrid: 'hybrid' }, },
    { title: "First name", field: "first_name", editable: 'never' },
    { title: "Last name", field: "last_name", editable: 'never' },
  ];

  const [rows, setRows] = useState([]);
  const [vehicles, setVehicles] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [newCustomerId, setNewCustomerId] = useState(-1);
  const [newVehicle, setNewVehicle] = useState({
    reg_plate: '',
    year: '',
    class: '',
    vin: '',
    transmissions: '',
    engine: '',
    model: '',
    brand: '',
  });
  const [isClicked, setIsClicked] = useState(false);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setRows(vehicles);
  }, [vehicles]);

  const getAllVehicles = () => {
    fetch(`${BASE_URL}/vehicles`, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setVehicles(result);
      })
      .catch((error) => console.log(error.message));
  };

  useEffect(() => {
    getAllVehicles();
  }, [isClicked]);

  useEffect(() => {
    fetch(`${BASE_URL}/users`, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        const arrCustomers = Object.values(result).map(({ id, full_name }) => [id, full_name])
        setCustomers(arrCustomers);

      })
      .catch((error) => console.log(error.message));

  }, [])

  const handleClickOpen = () => {
    setIsClicked(false);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const createVehicle = (newCustomerId, newVehicle) => {
    const transformNewVehicle = { ...newVehicle, customer_id: newCustomerId }

    fetch(`${BASE_URL}/vehicles`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
      body: JSON.stringify(transformNewVehicle),
    })
      .then((result) => result.json())
      .then((result) => {
        if (result.error) {
          toast.error(`${result.message}`, {
            position: toast.POSITION.TOP_CENTER,
            transition: Bounce,
            autoClose: true,
          })
        } else {
          setRows([...rows, result])
          setIsClicked(true)
        
          setNewVehicle({
            reg_plate: '',
            year: '',
            class: '',
            vin: '',
            transmissions: '',
            engine: '',
            model: '',
            brand: '',
          })
          toast.success(`You successfully created a vehicle!`, {
            position: toast.POSITION.TOP_CENTER,
            transition: Bounce,
            autoClose: true,
          })
        }
      })
      .then(() => setIsClicked(false))
      .catch((error) => {
        toast.error(`${error.message}`, {
          position: toast.POSITION.TOP_CENTER,
          transition: Bounce,
          autoClose: true,
        })
      })
  }

  const updateVehicle = (id, updatedVehicle) => {

    fetch(`${BASE_URL}/vehicles/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
      body: JSON.stringify(updatedVehicle),
    })
      .then((result) => result.json())
      .then((result) => {
        if (result.error) {
          toast.error(`${result.message}`, {
            position: toast.POSITION.TOP_CENTER,
            transition: Bounce,
            autoClose: true,
          })
        } else {
          setIsClicked(true)
          toast.success(`You successfully updated a vehicle!`, {
            position: toast.POSITION.TOP_CENTER,
            transition: Bounce,
            autoClose: true,
          })
        }
      })
      .catch((error) => {
        toast.error(`${error.message}`, {
          position: toast.POSITION.TOP_CENTER,
          transition: Bounce,
          autoClose: true,
        })
      })
  }

  const deleteVehicle = (id) => {
    fetch(`${BASE_URL}/vehicles/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
    })
      .then((result) => result.json())
      .then((result) => {
        if (result.error) {
          toast.error(`${result.message}`, {
            position: toast.POSITION.TOP_CENTER,
            transition: Bounce,
            autoClose: true,
          })
        } else {
          setIsClicked(true)
          toast.success(`You successfully deleted a vehicle!`, {
            position: toast.POSITION.TOP_CENTER,
            transition: Bounce,
            autoClose: true,
          })
        }
      })
      .then(() => setIsClicked(false))

  };

  const handleSubmit = () => {
    handleClose()
    createVehicle(newCustomerId, newVehicle)
  }

  const createVehicleProps = (prop, value) => {
    newVehicle[prop] = value;
    const created = { ...newVehicle };
    setNewVehicle(created);
  };

  return (
    <div>
      <Button variant="outlined" color="secondary" onClick={handleClickOpen}>
        Add a Vehicle
      </Button>
      <ToastContainer />
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title" style={{ textAlign: 'center' }}>Add a Vehicle</DialogTitle>
        <DialogContent>
          <DialogContentText style={{ textAlign: 'center' }}>
            Please, fill all the fields to create a new vehicle!
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="reg_plate"
            label="Reg.plate"
            fullWidth
            value={newVehicle.reg_plate}
            onChange={(e) => createVehicleProps("reg_plate", e.target.value)}
          />
          <TextField
            autoFocus
            margin="dense"
            id="year"
            label="Year"
            fullWidth
            value={newVehicle.year}
            onChange={(e) => createVehicleProps("year", e.target.value)}
          />
          <TextField
            autoFocus
            margin="dense"
            id="class"
            label="Class"
            fullWidth
            value={newVehicle.class}
            onChange={(e) => createVehicleProps("class", e.target.value)}
          />
          <TextField
            autoFocus
            margin="dense"
            id="vin"
            label="VIN"
            fullWidth
            value={newVehicle.vin}
            onChange={(e) => createVehicleProps("vin", e.target.value)}
          />
          <TextField
            autoFocus
            margin="dense"
            id="transmissions"
            label="Transmissions"
            fullWidth
            value={newVehicle.transmissions}
            onChange={(e) => createVehicleProps("transmissions", e.target.value)}
          />
          <TextField
            autoFocus
            margin="dense"
            id="engine"
            label="Engine"
            fullWidth
            value={newVehicle.engine}
            onChange={(e) => createVehicleProps("engine", e.target.value)}
          />
          <TextField
            autoFocus
            margin="dense"
            id="model"
            label="Model"
            fullWidth
            value={newVehicle.model}
            onChange={(e) => createVehicleProps("model", e.target.value)}
          />
          <Typography style={{ paddingRight: 25 }}>
            Brands:
           <Select style={{ width: 200, textAlign: 'center' }}
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              helperText="Select brand!"
              placeholder="Brand"
              value={newVehicle.brand}
              onChange={(e) => createVehicleProps("brand", e.target.value)}
            >
              {brands.map((brand, index) => (
                <MenuItem key={index} value={brand}>
                  {brand}
                </MenuItem>
              ))}
            </Select>
          </Typography>
          <Typography>
            Customers:
          <Select style={{ width: 200, textAlign: 'center' }}
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              helperText="Select brand!"
              onChange={(e) => setNewCustomerId(e.target.value)}
            >
              {customers.map((customer, index) => (
                <MenuItem key={index} value={customer[0]}>
                  {customer[1]}
                </MenuItem>
              ))}
            </Select>
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Cancel
          </Button>
          <Button onClick={handleSubmit} color="secondary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
      <MaterialTable
        icons={tableIcons}
        title="Vehicles"
        columns={columns}
        data={rows}
        editable={{
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                const updatedVehicle = {
                  reg_plate: newData.reg_plate,
                  year: newData.year,
                  class: newData.class,
                  transmissions: newData.transmissions,
                  engine: newData.engine,
                  model: newData.model,
                  brand: newData.brand,
                }
                updateVehicle(newData.id, updatedVehicle);
                const dataUpdate = [...rows];
                const index = oldData.tableData.id;
                dataUpdate[index] = newData;
                setRows([...dataUpdate]);
                resolve();
              }, 1000);
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                deleteVehicle(oldData.id);
                const dataDelete = [...rows];
                const index = oldData.tableData.id;
                dataDelete.splice(index, 1);
                setRows([...dataDelete]);
                resolve();
              }, 1000);
            }),
        }}
        options={{
          filtering: true,
          addRowPosition: 'first',
          tableLayout: "auto",
          padding: "dense",
          pageSize: 10,
          doubleHorizontalScroll: false,
          headerStyle: {
            width: 26,
            whiteSpace: "nowrap",
            textAlign: "left",
            flexDirection: "row",
            overflow: "hidden",
            textOverflow: "ellipsis",
            paddingLeft: 5,
            paddingRight: 5,
            backgroundColor: "#f50057",
            color: "whitesmoke",
            fontWeight: "bold",
            fontSize: 16,
            alignContent: 'center'
          },
          rowStyle: { backgroundColor: "#E5E5E5" },
        }}
      />
    </div>
  );
};

export default Vehicles;
