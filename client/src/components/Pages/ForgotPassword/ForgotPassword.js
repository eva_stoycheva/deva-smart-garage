import { validationSchemaForgot } from "../../../common/validators";
import { useStylesForgotPassword } from "./ForgotPasswordStyles";
import { ToastContainer, toast, Bounce } from 'react-toastify'
import { Formik, Form, Field, ErrorMessage } from "formik";
import { Copyright } from '../../Base/Copyright/Copyright';
import CssBaseline from "@material-ui/core/CssBaseline";
import { BASE_URL } from "../../../common/constants";
import TextField from "@material-ui/core/TextField";
import 'react-toastify/dist/ReactToastify.css';
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import React from "react";

const ForgotPassword = ({ history }) => {
  const classes = useStylesForgotPassword();
  const initialValues = {
    email: "",
  }

  const forgotPassword = (values, { setSubmitting }) => {

    fetch(`${BASE_URL}/forgot`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(values),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.error) {
          throw new Error(data.message)
        }
        toast.success('You successfully send a link!', {
          position: toast.POSITION.TOP_CENTER,
          transition: Bounce,
          autoClose: false,
          onOpen: () => setSubmitting(true),
          onClose: () => history.push('/login')
        })
      })
      .catch((error) => {
        toast.error(`${error.message}`, {
          position: toast.POSITION.TOP_CENTER,
          transition: Bounce,
          autoClose: false,
          onOpen: () => setSubmitting(true),
          onClose: () => setSubmitting(false)
        })
      })
  }

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={21}>
        <ToastContainer />
        <div className={classes.paper}>
          <h2 style={{ fontSize: "18px" }}>
            FORGOT YOUR PASSWORD?
          </h2>
          <Box mt={1}>
            <p style={{ fontSize: "18px" }}>
              Please enter the email address associated with your account and We will email you a link to reset your password.
          </p>
          </Box>
          <Formik
            initialValues={initialValues}
            onSubmit={forgotPassword}
            validationSchema={validationSchemaForgot}
          >
            {props => (
              <Form className={classes.form}>
                <Field
                  as={TextField}
                  variant="outlined"
                  name="email"
                  placeholder="Enter your email"
                  required
                  fullWidth
                  id="email"
                  label="Email"
                  type="email"
                  autoFocus
                  helperText={<ErrorMessage name="email" />}
                  error={props.errors.email ? true : false}
                  onChange={props.handleChange}
                />
                <Button
                  type="submit"
                  variant="outlined"
                  color="secondary"
                  className={classes.button}
                  disabled={props.isSubmitting}
                >
                  SEND LINK
                </Button>
                <Box mt={1}>
                  <p style={{ fontSize: "16px" }}>
                    Link will expire in a hour and could be used once!
                </p>
                  <Button
                    variant="outlined"
                    color="secondary"
                    className={classes.button}
                    onClick={() => { history.push('/login') }}
                  >
                    BACK
                </Button>
                </Box>
              </Form>
            )}
          </Formik>
          <Copyright />
        </div>
      </Grid>
    </Grid>
  );
};

export default ForgotPassword;
