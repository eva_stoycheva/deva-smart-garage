import { makeStyles } from "@material-ui/core/styles";

export const useStylesForgotPassword = makeStyles((theme) => ({
  root: {
    alignItems: "center",
    paddingLeft: "550px",
    paddingTop: "100px",
    width:"100%"

  },
  paper: {
    margin: theme.spacing(6, 4),
    display: "inline-flex",
    flexDirection: "column",
    alignItems: "center",
  },
  form: {
    width: "100%", 
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    display: "center",

  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },

    button: {
      margin: theme.spacing(3),
    },
}));
