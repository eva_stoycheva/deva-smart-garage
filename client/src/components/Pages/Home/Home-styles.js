import { makeStyles } from "@material-ui/core/styles";

export const useStylesHome = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundImage: `url('http://localhost:3000/service.jpg')`,
  },
  paper: {
    height: 140,
    width: 100,
  },
  control: {
    padding: theme.spacing(2),
  },
}));