import React from 'react';
import { Document, Page, Text, View, StyleSheet } from '@react-pdf/renderer';
import jsPDF from 'jspdf'
import { Button } from '@material-ui/core';
import mailgunFactory from 'mailgun-js';
// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: 'row',
    backgroundColor: '#E4E4E4'
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1
  }
});
 
// Create Document Component
const MyDocument = (data) => {
  
  const doc = jsPDF('landscape','px','a4','false')
   doc.text(20, 20, 'This is the default font.')

  
const DOMAIN = 'sandboxad454fea45484a0e8697d421eac1a5d3.mailgun.org';
const mailgunClient = mailgunFactory({apiKey: "419ad310b15e474132368ee03b9f6f85-fa6e84b7-961cf6c6", 
domain: DOMAIN});
const attachments = new mailgunClient.Attachment({data: doc, filename: `doc.pdf`,
 contentType: "application/pdf", })

const dataMail = {
	from: 'Deva <devasmartgarage@gmail.com>',
	to: 'deshka_113@abv.bg',
	subject: 'Hello',
	text: 'Testing some Mailgun awesomness!',
  attachment: attachments
};

const sendEmail=() =>{
 mailgunClient.messages().send(dataMail, function (error, body) {
  console.log(doc)
	console.log(body);
})
}

 return (
    <div>
   <Button onClick={sendEmail}>PDF</Button>
  </div>
  )
}
export default MyDocument;