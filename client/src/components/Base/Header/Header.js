import { AuthContext } from "../../../Context/AuthContext";
import { BASE_URL } from "../../../common/constants";
import { useHistory } from "react-router-dom";
import React, { useContext } from "react";
import "./Header.css";

const Header = () => {
  const auth = useContext(AuthContext);
  const history = useHistory();
  const handleClick = (path) => () => history.replace(path);

  const logout = () => {

    localStorage.removeItem(auth.user.email);

    const token = localStorage.getItem(auth.user.email);

    fetch(`${BASE_URL}/logout`, {
      method: "DELETE",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        authorization: "Bearer " + token,
      },
    })
      .catch((error) => console.log(error.message));
    localStorage.removeItem('token');
    auth.setAuthState({
      user: null,
      isLoggedIn: false,
    });

    history.replace('/home')
  };

  return (
    <div className="header">
      <button className="nav" onClick={handleClick("/home")}>
        HOME
      </button>
      { auth.isLoggedIn && auth.user.role === "customer" ? (
        <button className="nav" onClick={handleClick("/services")}>
          SERVICES
        </button>
      ) : ''}
      {auth.isLoggedIn && auth.user.role === "employee" ? (
        <button className="nav" onClick={handleClick("/dashboard")}>
          DASHBOARD
        </button>
      ) : ''}
      {!auth.isLoggedIn && (
        <button className="nav" onClick={handleClick("/login")}>
          LOGIN
        </button>)
      }
      {auth.isLoggedIn && <button className="nav" onClick={logout}>
        LOGOUT
        </button>}
      {auth.isLoggedIn && (
        <button className="nav" onClick={handleClick("/profile")}>
          PROFILE
        </button>
      )}
    </div>
  );
};

export default Header;
