import { createContext } from 'react';
import jwtDecode from 'jwt-decode'

export const AuthContext = createContext({
  isLoggedIn: false,
  user: null,
  role: null,
  setLoginState: () => {}
});

export const getUser = () => {
  try {
    return jwtDecode(localStorage.getItem('token') || '');
  } catch (error) {
    return null;
  }
};

